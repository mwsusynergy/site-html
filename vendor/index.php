<!DOCTYPE HTML>
<html>
<head>
	<title>MWSU Synergy</title>
	<link rel="stylesheet" type="text/css" href="../style.css">
	<link rel="shortcut icon" href="https://www.missouriwestern.edu/wp-content/themes/bretheon/images/favicon.ico" type="image/x-icon" />	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</head>
<body>
	<h1 id = "heading">Career Development Center</h1>
	<div id="navBar">
		<a class="welcome" href="http://webservices.missouriwestern.edu/users/amunsell1/public/js/synergy/">Welcome to the CDC!</a>
		<a class="welcome" href="http://webservices.missouriwestern.edu/users/amunsell1/public/js/synergy/login/">Student/Faculty/Staff</a>
	</div>
	
	<div id="mainBody">
	<img id="griffon" src="../logo.png" alt="MWSU Griffons"/>
	
	</div>
	
	<div id="footer">
	</div>
</body>
</html>