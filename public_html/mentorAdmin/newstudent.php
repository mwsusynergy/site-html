<?php
session_start();
if(!isset($_SESSION['userName']) || !isset($_SESSION['isAdmin'])){
	echo "<script>window.open('index.php','_self')</script>";
}

include("../../includes/header.html");
include("../../includes/db_connection.php");
?>
</div>
<p style='float:left; text-align:left;'><a href='admin.php'>&#8592; Admin Panel</a> | <a href='welcome.php'>Mentor Dashboard</a></p>

<p style='text-align:right'><a href='logout.php'>Logout</a></p><br />
<h1 class="content-subhead" >Create New Student Account</h1>
<div class="posts">

<form method="POST" action="create.php" name="createstudent" class="pure-form">

<input type="text" name="firstname" placeholder="First Name"><br /><br />
<input type="text" name="lastname" placeholder = "Last Name"><br /><br />
<input type="text" name="username" placeholder = "User Name"><br /><br />
<input type="text" name="gnumber" placeholder="G Number"><br /><br />
<select id="ethnicityreport" name="ethnicity" style="width:215px">
<option id="blank" name="Blank" disabled selected style="color:#999">Ethnicity</option>
<option id="hispaniclatino" name="Hispanic_Latino">Hispanic&#47;Latino</option>
<option id="amindian" name="American_Indian">American Indian&#47;Alaskan Native</option>
<option id="blackafrican" name="Black_African_American">Black&#47;African American</option>
<option id="hawaii" name="Pacific_Islander">Native Hawaiian&#47;Pacific Islander</option>
<option id="white" name="Caucasian">White&#47;Caucasian</option>
<option id="unknown" name="Unknown">Unknown</option>
</select><br /><br />
<input type="text" name="major" placeholder="Major"><br /><br />
<input type="text" name="grade" placeholder="Grade Level"><br /><br />
<button type="submit" name="submit" class="pure-button button-secondary">Submit</button>
</form>


<?php include("../../includes/footer.html"); ?>