<?php  
session_start();  
  
if(!$_SESSION['userName'])  
{  
  
    header("Location: index.php");//redirect to login page to secure the welcome page without login access.  
}  
 include("../../includes/header.html");
?>  
  </div>
  
<?php 

echo "<p style='text-align:right'><a href='logout.php'>Logout</a></p>";
?>

<h1 class="content-subhead">Mentor/Administrator Dashboard</h1>
<div class="posts">

<?php
$firstname = $_SESSION['firstName'];
$lastname = $_SESSION['lastName'];
$username= $_SESSION['userName'];
$isAdmin = $_SESSION['isAdmin'];
echo "<p>Welcome, $firstname.  Please select an option below to continue.</p>";
?>
 <a class="pure-button pure-button-primary" href="student.php">Student Dashboard</a><br /><br />
 <a class="pure-button pure-button-primary" href="vendor.php">Employer Dashboard</a><br /><br />
 <a class="pure-button pure-button-primary" href="alumni.php">Alumni Dashboard</a><br /><br />
 <?php
 if($isAdmin == 1){
 ?>
 <a class="pure-button pure-button-primary" href="admin.php">Administrator Panel</a><br /><br />
 <?php
 }
 ?>
  
<?php include("../../includes/footer.html"); ?>