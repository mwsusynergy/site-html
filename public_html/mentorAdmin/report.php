<?php
session_start();

if(!isset($_SESSION['userName']) && $_SESSION['isAdmin'] != 1){
	echo "<script>window.open('index.php','_self')</script>";
}
include("../../includes/header.html");
include("../../includes/db_connection.php");

?>
</div>
<p style='float:left; text-align:left;'><a href='studentreport.php'>&#8592; Report Selection</a> | <a href='admin.php'>Admin Panel</a></p>
<p style='text-align:right'><a href='logout.php'>Logout</a></p><br />
<h1 class="content-subhead">Reports</h1>
<div class="posts">
<?php
$selection = $_POST['selectionform'];


//DATE SELECTION
if($selection == "date"){
?>
<p>Select a Start Date and End Date:</p><br />
<form id="report" name="report" action="report.php" method="POST" class="pure-form">
<input name="startDate" type="date"><input name="endDate" type="date" style="margin-left:5em"><br /><br />
<button type="submit" name="submit" class="pure-button pure-button-primary">Generate</button>
</form>
<?php
}
if(isset($_POST['startDate']) && isset($_POST['endDate'])){

include("../../includes/admin/datereport.php");
}

//ETHNICITY SELECTION
if($selection == "ethnicity"){
	echo "<p>Select an Ethnicity:</p><br />";
?>
<form id="ethreport" name="ethreport" action="report.php" method="POST" class="pure-form">
<select id="ethnicityreport" name="ethnicityreport">
<option id="hispaniclatino" name="hispaniclatino">Hispanic&#47;Latino</option>
<option id="amindian" name="hispaniclatino">American Indian&#47;Alaskan Native</option>
<option id="blackafrican" name="blackafrican">Black&#47;African American</option>
<option id="hawaii" name="hawaii">Native Hawaiian&#47;Pacific Islander</option>
<option id="white" name="white">White&#47;Caucasian</option>
<option id="unknown" name="unknown">Unknown</option>
</select>
<button type="submit" name="submit" class="pure-button pure-button-primary">Generate</button>
</form>
<?php
}
if(isset($_POST['ethnicityreport'])){
include("../../includes/admin/ethreport.php");
}

//NAME SELECTION
if($selection == "name"){
	echo "<p>Enter the Name to Report On:<p>";

?>
<form id="inputForm" class="pure-form" action="report.php" method="POST" name="mainForm">
		<input type="text" name="firstName" autocomplete="off" placeholder="First Name"><br /><br />
		<input type="text" name="lastName" autocomplete="off" placeholder="Last Name"><br /><br />
		<button type="submit" name="submit" class="pure-button pure-button-primary">Sign in</button>
	</form>
	<script type="text/javascript">
		var frmvalidator  = new Validator("inputForm");
		frmvalidator.addValidation("firstName","req","Please enter your First Name");
		frmvalidator.addValidation("lastName","req","Please enter your Last Name");
	</script>
<?php
}
if(isset($_POST['firstName']) && isset($_POST['lastName'])){
	include("../../includes/admin/namereport.php");
}
include("../../includes/footer.html");
?>