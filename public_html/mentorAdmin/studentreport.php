<?php
	session_start();
	
	if(!isset($_SESSION['userName']) && $_SESSION['isAdmin'] == 1){
		echo "<script>window.open('index.php','_self')</script>";
	}
	
	include("../../includes/header.html");
	include("../../includes/db_connection.php");

?>


</div>
<p style='float:left; text-align:left;'><a href='admin.php'>&#8592; Admin Panel</a> | <a href='welcome.php'>Mentor Dashboard</a></p>
<p style='text-align:right'><a href='logout.php'>Logout</a></p><br />
<h1 class="content-subhead">Reports</h1>
<div class="posts">


<div id="form">
<p>Select a Value To Base Reports On:</p>
<form class="pure-form" action="report.php" method="POST" name='selection' id='selection'>
<select id="selectionform" name="selectionform">
<option value="blank" name="blank" id="blank">&nbsp;</option>
<option value="date" name="date" id="date">Date</option>
<!--<option value="ethnicity" name="ethnicity" id="ethnicity">Ethnicity</option>-->
<option value="name" name="name" id="name">Name</option>
</select><br />
<button type="submit" name="selectionsubmit" class="pure-button pure-button-primary">Continue</button>
</form>
</div>




<?php include("../../includes/footer.html"); ?>