<?php
	session_start();
	
	if(!isset($_SESSION['userName']) && $_SESSION['isAdmin'] != 1){
		echo "<script>window.open('index.php','_self')</script>";
	}
	
	include("../../includes/header.html");
	include("../../includes/db_connection.php");
?>
	
	

</div>
<p style='float:left; text-align:left;'><a href='admin.php'>&#8592; Admin Panel</a> | <a href='welcome.php'>Mentor Dashboard</a></p>
<p style='text-align:right'><a href='logout.php'>Logout</a></p><br />
<h1 class="content-subhead">Create Student Account</h1>
<div class="posts">
<div style="text-align:left; margin-left:40%;">
<p><b>Is this information correct?</b></p>


<?php
$firstname = $_POST['firstname'];
$lastname = $_POST['lastname'];
$username = $_POST['username'];
$gnumber = $_POST['gnumber'];
$ethnicitybefore = $_POST['ethnicity'];
$parts = explode("_",$ethnicitybefore);
$ethnicity = implode(' ',$parts);
$major = $_POST['major'];
$gradelevel = $_POST['grade'];
?>


<form name="confirmstudent" action="confirmcreate.php" method="POST">

<input type = "hidden" name="firstname" value="<?php echo $firstname; ?>">
<p>First Name: <?php echo $firstname; ?></p>
<input type= "hidden" name = "lastname" value="<?php echo $lastname; ?>">
<p>Last Name: <?php echo $lastname; ?></p>
<input type="hidden" name="username" value="<?php echo $username; ?>">
<p>User name: <?php echo $username; ?></p>
<input type="hidden" name="gnumber" value="<?php echo $gnumber; ?>">
<p>G Number: <?php echo $gnumber; ?></p>
<input type="hidden" name="ethnicity" value="<?php echo $ethnicity ?>">
<p>Ethnicity: <?php echo $ethnicity; ?></p>
<input type="hidden" name="major" value="<?php echo $major; ?>">
<p>Major: <?php echo $major; ?></p>
<input type="hidden" name="gradelevel"  value="<?php echo $gradelevel; ?>">
<p>Grade Level: <?php echo $gradelevel; ?></p>
<button type="submit" name="submit" class="pure-button button-secondary">Create Account</button>
<button onclick="goBack()" name="back" class="pure-button button-secondary">Go Back</button>

<script>
function goBack() {
    window.history.back();
}
</script>
</div>

</form>
<?php
include("../../includes/footer.html");
?>