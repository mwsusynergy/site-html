<div>
<div>
<p>Visit Reason:</p>
<form method="POST" action="finalize.php" name="<?php echo "$postvar"; ?>">
<select name='lookupTable' value="----">
    <option value="blank" name="blank" selected="selected">&nbsp;</option>
  <optgroup label="Resume">
    <option name="workingcampus" value="Resume_Working_On_Or_Off_Campus">Working on/off Campus</option>
    <option name="university101" value="Resume_University_101">University 101</option>
  </optgroup>
  <optgroup label="Griffon Link">
    <option name="accountproblem" value="Griffon_Link_Account_Problem">Problem with Account</option>
    <option name="newaccount" value="Griffon_Link_New_Account">New Account</option>
  </optgroup>
  <optgroup label="Career Advice">
    <option name="mockinterview" value="Career_Advice_Mock_Interview">Mock Interview</option>
    <option name="personalitytest" value="Career_Advice_Personality_Test">Personality Test</option>
    <option name="myplan" value="Career_Advice_My_Plan">My.Plan</option>
  </optgroup>
  <optgroup label="Student Employment">
    <option name="lookingforajob" value="Student_Employment_Looking_For_A_Job">Looking For a Job</option>
  </optgroup>
  <optgroup label="Learn, Serve, Change the World">
    <option name="pickingupinformation" value="Learn_Serve_Change_Picking_Up_Information">Picking Up Information</option>
    <option name="dropofftimesheets" value="Learn_Serve_Change_Drop_off_Time_Sheets">Drop Off Time Sheets</option>
    <option name="employervisit" value="Learn_Serve_Change_Employer_Visit">Employer Visit</option>
  </optgroup>
</select>
</div>
<br />
<br />
<input type="hidden" name="explodethis" value="<?php echo $postvar; ?>">
<textarea name="comment" rows="8" cols="50" placeholder="Comments"></textarea><br />

</div>
<button type='submit' name='finishMentor' class='pure-button pure-button-primary'>Finish</button>
</form>