<?php
session_start();
if(!$_SESSION['userName'])  
{  
  
    header("Location: index.php");//redirect to login page to secure the welcome page without login access.  
}
include("../../includes/header.html");
include("../../includes/db_connection.php");

if(!$_POST['alumni'])  
{  
    echo "<script>alert('This record does not exist.')</script>"; 
    echo "<script>window.open('alumni.php','_self')</script>";
}

$postvar = $_POST['alumni'];
$parts = explode("_",$postvar);

$firstname = $parts[0];
$lastname = $parts[1];
$timein = $parts[2];

if(isset($_POST['delete'])){
	
	

    	$queryDelete = "DELETE FROM `alumniIn` WHERE `lastName` = '$lastname' AND `timeIn` = '$timein'";
	mysqli_query($dbcon,$queryDelete);

	echo "<script>window.open('alumni.php','_self')</script>";
    

}


$theTime = date("Y-m-d h:i:s");

$query1 = "UPDATE `alumniIn` SET `TimeSeen` = '$theTime' WHERE `lastName` = '$lastname' AND `timeIn` = '$timein'";
mysqli_query($dbcon,$query1);

echo "</div>";
echo "<p style='float:left; text-align:left;'><a href='alumni.php'>&#8592; Back to Alumni Dash</a></p>";
echo "<p style='text-align:right'><a href='welcome.php'>Your Dashboard</a> | ";
echo "<a href='logout.php'>Logout</a></p>";
echo "<h1 class='content-subhead'>Alumni Help</h1>";
echo "<div class='posts'>";



echo "<div style='margin-left:25%; text-align:left;'>";


$query = "SELECT * FROM `alumniIn` WHERE `lastName` = '$lastname' AND `timeIn` = '$timein'";
$result = mysqli_query($dbcon,$query);
while($row = mysqli_fetch_assoc($result)){
	$firstname = $row['firstName'];
	$lastname = $row['lastName'];
	$account = $row['account'];
	$gradyear = $row['gradYear'];
	$timein = $row['timeIn'];
	
	echo "<p>$firstname $lastname</p>\n";
	echo "<p>Grad Year:  $gradyear";
	if($account == 1){
	echo "<p>New account needed.<p>\n";
	}else{
	echo "<p>New account <b>not</b> needed</p>\n";
	}
}
?>

<div>
<div>
<p>Visit Reason:</p>
<form method="POST" action="alumnifinalize.php" name="<?php echo "$postvar"; ?>">
<select name='lookupTable' value="----">
    <option value="blank" name="blank" selected="selected">&nbsp;</option>
  <optgroup label="Resume">
    <option name="workingcampus" value="Resume_Working_On_Or_Off_Campus">Working on/off Campus</option>
    <option name="university101" value="Resume_University_101">University 101</option>
  </optgroup>
  <optgroup label="Griffon Link">
    <option name="accountproblem" value="Griffon_Link_Account_Problem">Problem with Account</option>
    <option name="newaccount" value="Griffon_Link_New_Account">New Account</option>
  </optgroup>
  <optgroup label="Career Advice">
    <option name="mockinterview" value="Career_Advice_Mock_Interview">Mock Interview</option>
    <option name="personalitytest" value="Career_Advice_Personality_Test">Personality Test</option>
    <option name="myplan" value="Career_Advice_My_Plan">My.Plan</option>
  </optgroup>
  <optgroup label="Student Employment">
    <option name="lookingforajob" value="Student_Employment_Looking_For_A_Job">Looking For a Job</option>
  </optgroup>
  <optgroup label="Learn, Serve, Change the World">
    <option name="pickingupinformation" value="Learn_Serve_Change_Picking_Up_Information">Picking Up Information</option>
    <option name="dropofftimesheets" value="Learn_Serve_Change_Drop_off_Time_Sheets">Drop Off Time Sheets</option>
    <option name="employervisit" value="Learn_Serve_Change_Employer_Visit">Employer Visit</option>
  </optgroup>
</select>
</div>
<br />
<br />
<input type="hidden" name="explodethis" value="<?php echo $postvar; ?>">
<textarea name="comment" rows="8" cols="50" placeholder="Comments"></textarea><br />

</div>
<button type='submit' name='finishMentor' class='pure-button pure-button-primary'>Finish</button>
</form>
</div>

<?php
include("../../includes/footer.html");
?>