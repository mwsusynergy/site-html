<?php
	session_start();
	if(isset($_SESSION['userName'])){
		echo "<script>window.open('welcome.php','_self')</script>";
	}
	include("../../includes/header.html"); 
?>

<h1 class="content-subhead">Mentor/Administrator Login</h1>
<header class="post-header">
    <h2 class="post-title">Enter Your User Name and Password</h2>             
</header>
    
    <form id="loginForm" class="pure-form" action="index.php" method="POST" name="loginForm">
	    <input type="text" name="userName" autocomplete="off" placeholder="User Name"><br />
	    <input type="password" name="password" autocomplete="off" placeholder="Password"><br /><br />
	    <button type="submit" name="submit" class="pure-button pure-button-primary">Sign in</button>
    </form>
    
    <script type="text/javascript">
	    var frmvalidator  = new Validator("loginForm");
	    frmvalidator.addValidation("userName","req","Please enter your Username");
	    frmvalidator.addValidation("password","req","Please enter your Password");
    </script>
<?php  
  
	include("../../includes/db_connection.php");  
  
	if(isset($_POST['submit']))  
	{  
    		$user_name=htmlentities($_POST['userName']);  
    		$user_pass=htmlentities($_POST['password']);  
  
    		$check_user="SELECT * FROM `adminLogin` WHERE `username`='$user_name' AND `password`='$user_pass'";
  
    		$run=mysqli_query($dbcon,$check_user);  
  
   			if(mysqli_num_rows($run))  
    			{  
       				  
       				 
  				while ($row = mysqli_fetch_assoc($run)) {
        				$_SESSION['userName']=$user_name;
       					$_SESSION['firstName']=$row['firstname'];
       					$_SESSION['lastName']=$row['lastname'];
       					$_SESSION['isAdmin']=$row['isAdmin'];
       					echo "<script>window.open('welcome.php','_self')</script>"; 
    				}
       				 
  				 
    			}  
    			else  
    			{  
      				echo "<script>alert('Username or password is incorrect!')</script>";  
   			}  
	}  
?>  

<?php include("../../includes/footer.html"); ?>