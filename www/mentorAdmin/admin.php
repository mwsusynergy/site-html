<?php
	session_start();
	
	if(!isset($_SESSION['userName']) && $_SESSION['isAdmin'] != 1){
		echo "<script>window.open('index.php','_self')</script>";
	}
	
	include("../../includes/header.html");

?>
	</div>
  
<?php 
echo "<p style='float:left; text-align:left;'><a href='welcome.php'>&#8592; Back to Mentor Dash</a></p>";
echo "<p style='text-align:right'><a href='logout.php'>Logout</a></p><br />"; ?>

<h1 class="content-subhead">Administrator Panel</h1>
<div class="posts">

<?php
$firstname = $_SESSION['firstName'];
$lastname = $_SESSION['lastName'];
$username= $_SESSION['userName'];
$isAdmin = $_SESSION['isAdmin'];
?>
 <a class="pure-button button-secondary" href="studentreport.php">Student Report</a><br /><br />
 <a class="pure-button button-secondary" href="newstudent.php">New Student Account</a><br /><br />
 <a class="pure-button button-secondary" href="newmentor.php">New Mentor Account</a><br /><br />
 <a class="pure-button button-secondary" href="edit.php">Edit Student Account</a><br /><br />
 <a class="pure-button button-secondary" href="editmentor.php">Edit Mentor Account</a><br /><br />


<?php include("../../includes/footer.html");