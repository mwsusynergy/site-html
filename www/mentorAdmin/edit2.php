<?php 
session_start();
if(!isset($_SESSION['userName']) && $_SESSION['isAdmin'] != 1){
	echo "<script>window.open('index.php','_self')</script>";
}
include("../../includes/header.html");
include("../../includes/db_connection.php");

$gnumber = $_POST['gnumber'];
$query = "SELECT * FROM `studentList` WHERE `GNumber` = '$gnumber'";
$conx = mysqli_query($dbcon,$query);

while($row = mysqli_fetch_array($conx)){
	$firstname = $row['FirstName'];
	$lastname = $row['LastName'];
	$username = $row['UserName'];
	$gnumber = $row['GNumber'];
	$ethnicity = $row['Ethnicity'];
	$major = $row['Major'];
	$gradelevel = $row['GradeLevel'];
	$skills = $row['Skills'];
	$goal = $row['Goal'];
	$jobpref = $row['JobPref'];
	
	
}

?>
</div>
<p style='float:left; text-align:left;'><a href='edit.php'>&#8592; Student Edit Portal</a> | <a href='admin.php'>Admin Panel</a></p>
<p style='text-align:right'><a href='logout.php'>Logout</a></p><br />
<h1 class="content-subhead">Student Edit</h1>
<div class="posts">
<form name="editinfo" id="editinfo" class="pure-form" action="edit3.php" method="POST" style="text-align:left; margin-left:40%">
First Name: &emsp; <input type="text" name="firstname" value="<?php echo $firstname; ?>"><br /><br />
Last Name: &emsp; <input type="text" name="lastname" value="<?php echo $lastname; ?>"><br /><br />
User Name: &emsp; <input type="text" name="username" value="<?php echo $username; ?>"><br /><br />
G Number: &emsp; <input type="text" name="gnumber" value="<?php echo $gnumber; ?>"><br /><br />
Ethnicity:&emsp; <input type="text" name="ethnicity" value="<?php echo $ethnicity; ?>"><br /><br />
Major: &emsp; <input type="text" name="major" value="<?php echo $major; ?>"><br /><br />
Grade Level: &emsp; <input type="text" name="gradelevel" value="<?php echo $gradelevel; ?>"><br /><br />
Skills: &emsp; <input type="text" name="skills" value="<?php echo $skills; ?>"><br /><br />
Goal: &emsp; <input type="text" name="goal" value="<?php echo $goal; ?>"><br /><br />
Job Preference: &emsp; <input type="text" name="jobpref" value="<?php echo $jobpref; ?>"><br /><br />
<button type="submit" class="pure-button pure-button-primary">Submit</button>
</form>
<?php
include("../../includes/footer.html");
?>