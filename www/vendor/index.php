<?php 
session_start();
include("../../includes/header.html"); ?>

<h1 class="content-subhead">Employer Login</h1>
	<header class="post-header">
		<h2 class="post-title">Enter Your Information Into Our Database:</h2>             
	</header>
	<form id="employerInput" class="pure-form" action="confirm.php" method="POST" name="mainForm">
		<input type="text" name="firstName" autocomplete="off" placeholder="First Name">*<br />
		<input type="text" name="lastName" autocomplete="off" placeholder="Last Name">*<br />
		<input type="text" placeholder="Company Name" name="companyName" autocomplete="off">*<br />
		<input type="tel" placeholder="Phone" name="phone" autocomplete="off">*<br />
		<input type="text" placeholder="Position Title" name="positionTitle" autocomplete="off">&nbsp;<br />
		<input type="email" placeholder="Email" name="email" autocomplete="off">&nbsp;<br />
		<input type="tel" placeholder="Fax" name="fax" autocomplete="off">&nbsp;<br />
		<input type="text" placeholder="Company Address" name="address" autocomplete="off">&nbsp;<br /><br />
		Are you an alumni of Missouri Western State University? <input type="checkbox" name="alumni" value="1" /><br /><br />
		<button type="submit" name="submit" class="pure-button pure-button-primary">Submit</button>
	</form>
	<p><sub>* indicates a required field</sub></p>
	<script type="text/javascript">
		var frmvalidator  = new Validator("employerInput");
		frmvalidator.addValidation("firstName","req","Please enter your First Name");
		frmvalidator.addValidation("lastName","req","Please enter your Last Name");
		frmvalidator.addValidation("companyName","req","Please enter your Company Name");
		frmvalidator.addValidation("phone","req","Please enter your Phone Number");
	</script>
	
<?php
/*include("../../includes/db_student.php");  
  
	if(isset($_POST['submit']))  
	{  
    		$user_name=htmlentities($_POST['lastName']);  
    		$user_pass=htmlentities($_POST['gNumber']);  
  
    		$check_user="SELECT * FROM `studentList` WHERE `LastName`='$user_name' AND `GNumber`='$user_pass'";
  
    		$run=mysqli_query($dbcon,$check_user);  
  		
   			
   			if(mysqli_num_rows($run))  
    			{  
    				
    				while ($row = mysqli_fetch_assoc($run)) {
        				
       					$_SESSION['firstName']=$row['FirstName'];
       					$_SESSION['userName']=$row['UserName'];
    				}

       				echo "<script>window.open('confirm.php','_self')</script>";  
       				 
  				$_SESSION['gNumber']=$user_pass;
       				$_SESSION['lastName']=$user_name;
       				
       				
       			
  
    			}  
    			else  
    			{  
      				echo "<script>alert('Username or password is incorrect!')</script>";  
   			}  
	}*/

?>


<?php include("../../includes/footer.html"); ?>