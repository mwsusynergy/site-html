<?php include("../../includes/header.html"); ?>

<h1 class="content-subhead">Alumni Login</h1>
	<header class="post-header">
		<h2 class="post-title">Please Enter Your Login Information:</h2>            
	</header><br />
	<form id="alumniInput" class="pure-form" action="confirm.php" method="POST" name="mainForm">
		<input type="text" name="firstName" autocomplete="off" placeholder="First Name">*<br />
		<input type="text" name="lastName" autocomplete="off" placeholder="Last Name">*<br />
		Grad Year:
		<?php
			$current_year = date("Y");
			$range = range(($current_year), ($current_year-100));
			echo '<select name="gradyear" id="contact-year" tabindex="7">';
			 
			//Now we use a foreach loop and build the option tags
			foreach($range as $r)
			{
			echo '<option value="'.$r.'">'.$r.'</option>';
			}
			 
			//Echo the closing select tag
			echo '</select><br />';
		?>
		
		Do you need a new account? <input type="checkbox" name="newacct" value="1" /><br /><br />
		<button type="submit" name="submit" class="pure-button pure-button-primary">Submit</button>
	</form>
	<p><sub>* indicates a required field</sub></p>
	<script type="text/javascript">
		var frmvalidator  = new Validator("alumniInput");
		frmvalidator.addValidation("firstName","req","Please enter your First Name");
		frmvalidator.addValidation("lastName","req","Please enter your Last Name");
		frmvalidator.addValidation("gradyear","req","Please enter your Company Name");
	</script>

<?php include("../../includes/footer.html"); ?>