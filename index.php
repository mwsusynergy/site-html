<!DOCTYPE HTML>
<html>
<head>
	<title>MWSU Synergy</title>
	<meta charset="utf-8"/>
	<link rel="stylesheet" type="text/css" href="style.css">
	<link rel="shortcut icon" href="https://www.missouriwestern.edu/wp-content/themes/bretheon/images/favicon.ico" type="image/x-icon" />	
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
</head>
<body>
	<div id="wholeTop">
		<img id="logoOnly" src="logoOnly.png" alt="full logo" />
		<h1 id = "heading">Career Development Center</h1>
		<div id="navBar">
			<a class="welcome" href="http://webservices.missouriwestern.edu/users/amunsell1/public/js/synergy/"><img src="buttonHome.png" alt="Home"></a>
			<a class="welcome" href="http://webservices.missouriwestern.edu/users/amunsell1/public/js/synergy/login/"><img src="buttonLogin.png" alt="Login"></a>
		</div>
	</div>
	
	
	<div id="wholeBody">
		<img id="griffon" src="logo.png" alt="MWSU Griffons"/>
			<div id="mainBody">
				<h3>Welcome to the CDC!</h3>
				<p>Please Select:</p>
				<a class="selectButtons" href="http://webservices.missouriwestern.edu/users/amunsell1/public/js/synergy/login/">Student/Faculty/Staff</a>
				<a class="selectButtons" href="http://webservices.missouriwestern.edu/users/amunsell1/public/js/synergy/vendor/">Vendor</a>
				<a class="selectButtons" href="http://webservices.missouriwestern.edu/users/amunsell1/public/js/synergy/mentorAdmin/">Mentor/Admin</a>
			</div>
	</div>
	<div id="footer">
	
	</div>
</body>
</html>